package com.sele2.testcases.DA_LOGIN;

import org.testng.annotations.Test;
import com.sele2.support.Constant;
import com.sele2.helper.Log;
import com.sele2.testcases.testbase.TestBase;

public class DA_LOGIN_TC002 extends TestBase{
	@Test(description = "DA_LOGIN_TC002: Verify that user fails to login specific repository successfully via Dashboard login page with incorrect credentials")
    public void DA_LOGIN_TC002() {
		goToDashboardLoginPage();
        Log.info("Step 2: Enter invalid username and password");
        Log.info("Step 3: Click on Login button");
        loginPage.login(Constant.REPOSITORY, Constant.INVALID_USERNAME, Constant.INVALID_PASSWORD);

        Log.info("VP: Verify that Dashboard Error message 'Username or password is invalid' appears");
        softAssert.assertEquals(loginPage.getLoginErrorMessage(), Constant.ERROR_MESSAGE_INVALID_USERNAME_OR_PASSWORD);
    }
}
